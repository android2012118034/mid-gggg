package com.susan.new_mid;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class Fragment02 extends Fragment {
    TextView nf, nu, wf, wu;
    String databaseName = "data.txt";
    String tableName = "data";
    SQLiteDatabase database;
    Cursor cursor;
    View view;
    Button b;
    int flag=0;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag02, container, false);
        nf = (TextView) view.findViewById(R.id.needfun);
        nu = (TextView) view.findViewById(R.id.needurgent);
        wu = (TextView) view.findViewById(R.id.wanturgent);
        wf = (TextView) view.findViewById(R.id.wantfun);

        b = (Button) view.findViewById(R.id.button);

        database = getActivity().openOrCreateDatabase(databaseName,
                Context.MODE_PRIVATE, null);
        //println("database open"+databaseName);

        Log.i("****", databaseName.toString());

        if (database != null) {
            database.execSQL("CREATE TABLE if not exists " + tableName + "("
                    + "_id integer PRIMARY KEY autoincrement, "
                    + "data text, "
                    + "fun integer, " + "want integer, " + "urgent integer, " + "need integer"
                    + ")");
            //println("database create" + tableName);
        } else {
            println("데이터베이스를 열어야한다");
        }
        Log.i("******", tableName.toString());


        view.findViewById(R.id.button).setOnClickListener(mClickListener);
        return view;
    }


    View.OnClickListener mClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {

           /* database = getActivity().openOrCreateDatabase(databaseName,
                    Context.MODE_PRIVATE, null);
            //println("database open"+databaseName);


            Log.i("****", databaseName.toString());

            if (database != null) {
                database.execSQL("CREATE TABLE if not exists " + tableName + "("
                        + "_id integer PRIMARY KEY autoincrement, "
                        + "data text, "
                        + "fun integer, " + "want integer, " + "urgent integer, " + "need integer"
                        + ")");
                //println("database create" + tableName);
            } else {
                println("데이터베이스를 열어야한다");
            }*/

			/*if(database != null) {
                cursor = database.rawQuery("SELECT data, fun, want, urgent, need FROM " + tableName, null);
				int count = cursor.getCount();
				Log.i("******count",String.valueOf(count) );
				Log.i("******flag",String.valueOf(flag) );
				Log.i("******data", cursor.getString(0) );
				//println("데이터 조회 : " + count + "개");
				if (count > flag) {
					cursor.moveToNext();
					String data = cursor.getString(0);
					int fun = cursor.getInt(1);	//1000
					int want = cursor.getInt(2);	//100
					int urgent = cursor.getInt(3);	//10
					int need = cursor.getInt(4);	//1
					int a = fun + want + urgent + need;
					Log.i("******f:",String.valueOf(fun) );
					Log.i("******w:",String.valueOf(want) );
					Log.i("******u:",String.valueOf(urgent) );
					Log.i("******n:",String.valueOf(need) );
					Log.i("******=>a:",String.valueOf(a) );
					if (a == 11) {    //un -2
						println2("#" + count + ": " + data);
					} else if (a == 101) { //wn -.. ->1
						println1("#" + count + ": " + data);
					} else if (a == 1001) {    //fn -1
						println1("#" + count + ": " + data);
					} else if (a == 1100) {    //fw-4
						println4("#" + count + ": " + data);
					} else if (a == 1010) {    //fu-..->3
						println3("#" + count + ": " + data);
					} else if (a == 101) {        //wn-..->1
						println1("#" +count + ": " + data);
					} else if (a == 110) {        //wu-3
						println3("#" + count + ": " + data);
					}
					flag = count;
				cursor.close();
			}
				Log.i("*******","end");
			}else{
				println("데이터를 열어야 합니다");
			}*/

            if (database != null) {
                cursor = database.rawQuery("SELECT data, fun, want, urgent, need FROM " + tableName, null);
                int count = cursor.getCount();
                //println("데이터 조회 : " + count + "개");
                if (flag == 0) {
                    flag = 1;
                    for (int i = 0; i < count; i++) {
                        cursor.moveToNext();
                        String data = cursor.getString(0);
                        int fun = cursor.getInt(4);
                        int want = cursor.getInt(3);
                        int urgent = cursor.getInt(2);
                        int need = cursor.getInt(1);
                        int a = fun + want + urgent + need;
                        Log.i("******data", cursor.getString(0));
                        Log.i("******1f:", String.valueOf(fun));
                        Log.i("******1w:", String.valueOf(want));
                        Log.i("******1u:", String.valueOf(urgent));
                        Log.i("******1n:", String.valueOf(need));
                        Log.i("******=>1a:", String.valueOf(a));
                        if (a == 11) {    //un -2
                            println4(data);
                        } else if (a == 101) { //wn -.. ->1
                            println1(data);
                        } else if (a == 1001) {    //fn -1
                            println1(data);
                        } else if (a == 1100) {    //fw-4
                            println2(data);
                        } else if (a == 1010) {    //fu-..->3
                            println3(data);
                        } else if (a == 101) {        //wn-..->1
                            println1(data);
                        } else if (a == 110) {        //wu-3
                            println3(data);
                        }

                    }
                    cursor.close();

                } else {
                    //database.rawQuery("DELETE ")
                   nf.setText("해야하지만 재밌는 일");
                    nu.setText("급하게 해야하는 일");
                    wu.setText("하고 싶지만 급한 일");
                    wf.setText("하고 싶고 재밌는 일");
                    /*for (int i = 0; i < count; i++) {
                        cursor.moveToNext();
                        String data = cursor.getString(0);
                        int fun = cursor.getInt(4);
                        int want = cursor.getInt(3);
                        int urgent = cursor.getInt(2);
                        int need = cursor.getInt(1);
                        int a = fun + want + urgent + need;
                        Log.i("******data", cursor.getString(0));
                        Log.i("******1f:", String.valueOf(fun));
                        Log.i("******1w:", String.valueOf(want));
                        Log.i("******1u:", String.valueOf(urgent));
                        Log.i("******1n:", String.valueOf(need));
                        Log.i("******=>1a:", String.valueOf(a));
                        if (a == 11) {    //un -2
                            println2(data);
                        } else if (a == 101) { //wn -.. ->1
                            println1(data);
                        } else if (a == 1001) {    //fn -1
                            println1(data);
                        } else if (a == 1100) {    //fw-4
                            println4(data);
                        } else if (a == 1010) {    //fu-..->3
                            println3(data);
                        } else if (a == 101) {        //wn-..->1
                            println1(data);
                        } else if (a == 110) {        //wu-3
                            println3(data);
                        }
                    }
                    cursor.close();*/
                    flag = 0;
                }
                Log.i("*******1", "end");
            } else {
                println("데이터를 열어야 합니다");
            }

        }
    };

    private void println1(String data) {
        nf = (TextView) view.findViewById(R.id.needfun);
        nf.append("\nnf" + data + "\n");
    }

    private void println2(String data) {
        nu = (TextView) view.findViewById(R.id.needurgent);
        nu.append("\n" + data + "\n");
    }

    private void println3(String data) {

        wu = (TextView) view.findViewById(R.id.wanturgent);
        wu.append("\nwu" + data + "\n");
    }

    private void println4(String data) {
        wf = (TextView) view.findViewById(R.id.wantfun);
        wf.append("\n" + data + "\n");
    }

    private void println(String data) {
        nf = (TextView) view.findViewById(R.id.needfun);
        nf.append("\n--" + data + "\n");
    }

}