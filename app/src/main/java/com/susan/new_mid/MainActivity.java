package com.susan.new_mid;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ActionBar.TabListener {

    SectionsPagerAdapter mSectionsPagerAdapter;

    ViewPager mViewPager;
    TextView nf, nu, wf, wu;
    EditText add;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckBox cb = (CheckBox) findViewById(R.id.listrow_checkbox);

        final ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });


        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            actionBar.addTab(actionBar.newTab()
                    .setText(mSectionsPagerAdapter.getPageTitle(i))
                    .setTabListener(this));
        }
    }

    public void onStart() {

        pref = getPreferences(Context.MODE_PRIVATE);


        add = (EditText)findViewById(R.id.add_edit);
        //add.setText(pref.getString("data",""));
        nf = (TextView)findViewById(R.id.needfun);

        nu = (TextView)findViewById(R.id.needurgent);
        wf = (TextView)findViewById(R.id.wantfun);
        wu = (TextView)findViewById(R.id.wanturgent);

        super.onStart();
    }

    @Override
    public void onStop() {
        //SharedPreferences.Editor edit = pref.edit();
        super.onStop();


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, ShareActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_help) {
            Intent intent = new Intent(this, HelpActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    String [] man = {
            "",
            "배는 항구에 정박해 있을때 가장 안전하다.그러나 그것이 배의 존재 이유는 아니다",
            "희망이 도망치더라도 용기를 잃어서는 안된다",
            "누구도 당신을 먼저 끼워주지 않는다",
            "자신을 믿어라. 자신의 능력을 신뢰하라. 겸손하지만 합리적인 자신감없이는 성공할 수도, 행복할 수도 없다",
            "나처럼 되고 싶다고 하는 사람들이 있던데 그들은 자기처럼 되어야 한다",
            "네가 현재 서 있는 곳에서, 네가 가진 것으로 네가 할 수 있는 최선을 다하라",
            "어두운 구름 뒤에는 빛나는 별이 있다",
            "실패할 때마다 '그것봐라'는 식으로 보기 때문에 '역시'하고 쉽게 포기하면 자라날 싹도 자랄 수 없다",
            "나는 당신이 무너지더라도 걱정하지 않는다. 나는 당신이 다시 일어설 것인가를 걱정한다",
            "아무것도 안하는 건 쉽겠지만 너를 불행하게 만들 것이다",
            "당신이 사랑하는 삶을 살라. 당신이 사는 삶을 사랑하라",
            "당신이 시대에 속해 있다는 것을 기억하라. 우리의 작은 인생이 모여 하나의 시대와 문화가 된다",
            "네가 누군가에게 내린 평가는 그 사람에 대한 이야기가 아니라 자기소개다",
            "정말 자존감 높은 사람은 아무 옷이나 사 입어요. 명품 그런거 안 따지고. 근데 멋있게 보이죠",
            "부패는 자기 한계에 대한 고백이다. 일종의 무릎꿇음이다",
            "적이 강하면 더 제대로 덤벼들어야 한다",
            "영웅을 믿는 일이 영웅을 만들어 낸다",
            "어디로 가고 있는가?\n그곳에 도달하기 위해 오늘 무엇을 했는가?"
    };
    int i=0;
    public void onClickNew(View view) {
        i++;
        TextView tv = (TextView)findViewById(R.id.tv3);
        if(man.length>i)
            tv.setText(man[i]);
        else
            i=0;
    }



    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
//            // getItem is called to instantiate the fragment for the given page.
//            // Return a PlaceholderFragment (defined as a static inner class below).
//            return PlaceholderFragment.newInstance(position + 1);
            Fragment frag = null;


            if (position == 0) {
                frag = new Fragment01();
            } else if (position == 1) {
                frag = new Fragment02();
                //getItem2(1);
            } else if (position == 2) {
                frag = new Fragment03();
            }

            return frag;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }

}
