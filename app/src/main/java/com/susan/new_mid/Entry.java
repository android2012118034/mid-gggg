package com.susan.new_mid;

/**
 * Created by apple27 on 2015. 10. 28..
 */

public class Entry {
    static int pos = 0;
    //false == 1
    String name = null;
    int need = 1;
    int urgent = 1;
    int want = 1;
    int fun = 1;

    public Entry() {
        //pos++;
    }

    @Override
    public String toString() {
        return name;
    }

    //public Entry(String name, int need, int urgent, int want, int fun){
    public Entry(String name, int fun, int want, int urgent, int need) {
        super();
        this.name = name;
        this.fun = fun;
        this.need = need;
        this.urgent = urgent;
        this.want = want;
        pos++;
    }

    public int getPos() {
        return pos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int isNeed() {
        if (need == 0) {
            return 1;
        }
        return 0;
    }

    public void setNeed(int need) {
        this.need = need;
    }

    public int isUrgent() {
        if (need == 0) {
            return 10;
        }
        return 0;
    }

    public void setUrgent(int urgent) {
        this.urgent = urgent;
    }

    public int isFun() {
        if (fun == 0) {
            return 1000;
        }
        return 0;
    }

    public void setFun(int fun) {
        this.fun = fun;
    }

    public int isWant() {
        if (want == 0) {
            return 100;
        }
        return 0;
    }

    public void setWant(int want) {
        this.want = want;
    }

}
