package com.susan.new_mid;


import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class Fragment01 extends Fragment implements View.OnClickListener {
    int flag=0;
    private AbsListView.MultiChoiceModeListener mListener = null;
    ArrayAdapter<Entry> adapName = null;
    ArrayList<Entry> list = null;
    ListView listView;
    View view;
    Bundle arg;

    int pos;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag01, container, false);

        arg = new Bundle();
        //Log.i("****",String.valueOf(getArguments()));
        Context c = this.getActivity().getApplicationContext();

        //리스트뷰의 데이터를 저장할 어댑터 생성
        listView = (ListView) view.findViewById(R.id.listView);
        list = new ArrayList<Entry>();
        adapName = new ArrayAdapter<Entry>
                (c, R.layout.listrow, R.id.listrow_text, list);
        listView.setAdapter(adapName);

        /*Entry entry = new Entry(getString(R.string.tutorial_01), 1, 1, 0, 0);
        list.add(entry);

       entry = new Entry(getString(R.string.tutorial_02), 1, 0, 1, 0);
        list.add(entry);
        entry = new Entry(getString(R.string.tutorial_03), 1, 1, 0, 0);
        list.add(entry);
        entry = new Entry(getString(R.string.tutorial_04), 0, 0, 1, 1);
        list.add(entry);

*/
        if(getArguments()!= null){
            Entry entry = new Entry(getArguments().getString("data"),1,1,0,0);
            list.add(entry);
            Log.i("****",String.valueOf(arg));
            flag = 1;
        }

        //pos = listView.getCheckedItemPosition();
        //Toast toast = Toast.makeText(getActivity(),String.valueOf(pos),Toast.LENGTH_SHORT);
        //toast.show();


        //리스트뷰의 이벤트 설정
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listView.setOnItemClickListener(new ListViewItemClickListener());


        view.findViewById(R.id.menu_add).setOnClickListener(mClickListener);
        view.findViewById(R.id.menu_clear_completed).setOnClickListener(mClickListener);

        return view;
    }

   /* @Override
    public void onStart() {

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sp_data = sp.getString("data",getArguments().getString("data"));

        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();


    }*/

    ImageButton.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()){
                case R.id.menu_add: //추가버튼
                    FragmentAdd fr = new FragmentAdd();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fm.beginTransaction();
                    fragmentTransaction.replace(R.id.main_root, fr);
                    fragmentTransaction.commit();
                    break;
                case R.id.menu_clear_completed: //삭제버튼
                    CheckBox ck = (CheckBox)view.findViewById(R.id.listrow_checkbox);
                    pos = listView.getCheckedItemPosition();
                    pos = 0;
                    //Toast toast = Toast.makeText(getActivity(),String.valueOf(pos),Toast.LENGTH_SHORT);
                    //toast.show();
                    if (pos != ListView.INVALID_POSITION) {
                        list.remove(pos);
                        listView.clearChoices();

                        adapName.notifyDataSetChanged();
                    }
                    break;
            }
        }
    };

    private class ListViewItemClickListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            AlertDialog.Builder alertDlg = new AlertDialog.Builder(view.getContext());
            alertDlg.setPositiveButton("확인", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();  // AlertDialog를 닫는다.
                }
            });

            //alertDlg.setMessage(list.get(1));
            //alertDlg.show();
        }
    }
    int selectedPos = -1;

   /* AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener(){

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            String mes = "Select item = "+ list.get(i);
            Toast.makeText(getActivity(),mes,Toast.LENGTH_SHORT).show();
        }
    };*/
    @Override
    public void onClick(View v) {


    }

}