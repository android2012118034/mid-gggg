package com.susan.new_mid;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAdd extends Fragment {
    TextView nf, nu, wf, wu;
    EditText a;
    String databaseName = "data.txt";
    String tableName = "data";
    SQLiteDatabase database;
    ArrayAdapter<Entry> adapName = null;
    ArrayList<Entry> list = null;
    ListView listView;
    View view;
    Context c;
    CheckBox cb1, cb2, cb3, cb4;
    EditText ev;


    //Bundle arg;

    public FragmentAdd() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle arg) {

        c = getActivity().getApplicationContext();

        view = inflater.inflate(R.layout.frag_add, container, false);

        list = new ArrayList<Entry>();
        adapName = new ArrayAdapter<Entry>(
                getActivity(), R.layout.listrow, R.id.listrow_text, list
        );

        a = (EditText)view.findViewById(R.id.editText);
        cb1 = (CheckBox) view.findViewById(R.id.need);
        cb2 = (CheckBox) view.findViewById(R.id.urgent);
        cb3 = (CheckBox) view.findViewById(R.id.want);
        cb4 = (CheckBox) view.findViewById(R.id.fun);
        ev = (EditText) view.findViewById(R.id.add_edit);

        Button add = (Button) view.findViewById(R.id.add_add);
/*
        InputMethodManager imm =
                (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(),0);*/
        add.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                Bundle arg = new Bundle();

                Entry e = null;
                int a = 0;

                if (cb1.isChecked() == true)    //need
                    a += 1;
                if (cb2.isChecked() == true)    //urgent
                    a += 10;
                if (cb3.isChecked() == true)    //want
                    a += 100;
                if (cb4.isChecked() == true)    //fun
                    a += 1000;

                Log.i("*****ADD_a", String.valueOf(a));
                //Toast msg1= Toast.makeText(this,
                //       String.valueOf(a), Toast.LENGTH_SHORT);
                //msg1.show();
                //fun want urgent need
                String[] tmp = null;
                if (ev.getText().length() != 0) {
                    if (a == 11) {
                        e = new Entry(ev.getText().toString(), 0,0,1,1);
                    } else if (a == 101) {
                        e = new Entry(ev.getText().toString(), 0,1,0,1);
                    } else if (a == 1001) {
                        e = new Entry(ev.getText().toString(), 1,0,0,1);
                    } else if (a == 1100) {
                        e = new Entry(ev.getText().toString(), 1,1,0,0);
                    } else if (a == 1010) {
                        e = new Entry(ev.getText().toString(), 1,0,1,0);

                    } else if (a == 101) {
                        e = new Entry(ev.getText().toString(), 0,1,0,1);
                        // arg.putString("data", ev.getText().toString());

                    } else if (a == 110) {
                        e = new Entry(ev.getText().toString(),0,1,1,0);
                        // arg.putString("data", ev.getText().toString());

                    } else {
                        e = null;
                        Toast msg = Toast.makeText(getActivity(),
                                "두 개의 체크박스를 체크해주세요", Toast.LENGTH_LONG);
                        msg.show();
                        Fragment01 fr = new Fragment01();
                        FragmentManager fm = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fm.beginTransaction();
                        fragmentTransaction.replace(R.id.linear_add, fr);

                        fragmentTransaction.commit();
                    }
                    database = getActivity().openOrCreateDatabase(databaseName,
                            Context.MODE_PRIVATE, null);
                    println("database open"+databaseName);
                    if (database != null) {
                        database.execSQL("CREATE TABLE if not exists " + tableName + "("
                                + "_id integer PRIMARY KEY autoincrement, "
                                + "data text, "
                                + "fun integer, " + "want integer, " + "urgent integer, " + "need integer"
                                + ")");
                        println("database create" + tableName);
                    }else {
                        println("데이터베이스를 열어야한다");
                    }

                    if(tableName != null) {
                        if (database != null) {
                            database.execSQL("INSERT INTO " + tableName + "(data, fun, want, urgent, need) VALUES "
                                    + "('"+e.getName()+"', "+e.isFun()+", "+e.isWant()+", "+ e.isUrgent()+", "+e.isNeed()+");");

                            println("데이터를 추가했습니다");
                        } else {
                            println("데이터베이스를 열어야한다");
                        }
                    }
                    arg.putString("data", ev.getText().toString());
                    Log.i("*****", String.valueOf(arg));
                }else {
                    println("이름을 입력하세요");
                }
                Log.i("*****", String.valueOf(e.getName()));
                //if(e != null)
                //  arrName.add(e);
                //ev.setText("");
                //adapName.notifyDataSetChanged();


                Fragment01 fr = new Fragment01();
                if (e.getName() != null) {
                    fr.setArguments(arg);
                }
                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();

                fragmentTransaction.replace(R.id.linear_add, fr);

                fragmentTransaction.commitAllowingStateLoss();


                //Intent intent = new Intent(c, Fragment01.class);
                //c.startActivity(intent);


            }


        });

        Button back = (Button) view.findViewById(R.id.add_back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment01 fr = new Fragment01();

                FragmentManager fm = getFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.linear_add, fr);

                fragmentTransaction.commit();
            }


        });


        return view;
    }


    private void println(String data){
        a = (EditText)view.findViewById(R.id.editText);
        /*nf = (TextView)view.findViewById(R.id.needfun);
        nu = (TextView)view.findViewById(R.id.needurgent);
        wf = (TextView)view.findViewById(R.id.wantfun);
        wu = (TextView)view.findViewById(R.id.wanturgent);*/
       a.append(data + "\n");
    }
}
